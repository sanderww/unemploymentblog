### flask based blog app

This a flask based blog website I created as a learning project while I emigrated and was waiting for my working visa to be approved. I've used it to share stories with friends and family on my journey while learning python.

The app allows you to create HTML based blog posts which are stored in a postgress db. There's an admin role to create blogs and a viewer role for readers. The blog can be hosted for low costs or for free on platforms like heroku