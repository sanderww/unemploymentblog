from flask import render_template, request, Blueprint, flash, redirect, url_for
from flaskblog.models import Post, Subscription
from flaskblog.main.forms import SubscriptionForm
from flask_login import login_required
from flaskblog.main.utils import send_subscription_token
from flaskblog import db


main = Blueprint('main', __name__)


@main.route("/", methods=['GET', 'POST'])
@main.route("/home", methods=['GET', 'POST'])
def home():
    page = request.args.get('page', 1, type=int)
    posts = Post.query.order_by(Post.date_posted.desc()).paginate(page=page, per_page=6)
    subscriptionform = SubscriptionForm()
    if subscriptionform.validate_on_submit():
        subscription = Subscription(email=subscriptionform.email.data, reason=subscriptionform.reason.data,
                                    status='Pending')
        is_subscribed = Subscription.query.filter_by(email=subscription.email).first()
        if is_subscribed:
            flash('You are already subscribed to it', 'danger')
            return redirect(url_for('main.home'))
        else:
            send_subscription_token(subscription)
            db.session.add(subscription)
            db.session.commit()
            flash('An email has been sent with instructions to subscribe (to it).', 'info')
            return redirect(url_for('main.home'))
    return render_template('home.html', posts=posts, form=subscriptionform)


@main.route("/subscribe", methods=['GET', 'POST'])
def subscribe():
    subscriptionform = SubscriptionForm()
    if subscriptionform.validate_on_submit():
        subscription = Subscription(email=subscriptionform.email.data, reason=subscriptionform.reason.data,
                                    status='Pending')
        is_subscribed = Subscription.query.filter_by(email=subscription.email).first()
        if is_subscribed:
            flash('You are already subscribed to it', 'danger')
            return redirect(url_for('main.home'))
        else:
            send_subscription_token(subscription)
            db.session.add(subscription)
            db.session.commit()
            flash('An email has been sent with instructions to subscribe (to it).', 'info')
            return redirect(url_for('main.home'))
    return render_template('subscribe.html', form=subscriptionform)


@main.route("/confirm_subscription/<token>", methods=['GET', 'POST'])
def reset_token(token):
    subscription_email = Subscription.verify_reset_token(token)
    if subscription_email is None:
        flash('That is an invalid or expired token', 'warning')
        return redirect(url_for('main.home'))
    else:
        subscription = Subscription.query.filter_by(email=subscription_email).first()
        subscription.status = 'Active'
        db.session.commit()
        flash('You are definitely subscribed to it', 'success')
        return redirect(url_for('main.home'))

@main.route("/about")
def about():
    return render_template('about.html', title='About')


@main.route("/subscriptions")
@login_required
def subscriptions():
    subscription_emails = Subscription.query.all()
    return render_template('subscriptions.html', title='Subscriptions', subscription=subscription_emails)

