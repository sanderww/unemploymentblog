from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, TextAreaField
from wtforms.validators import DataRequired, Email


class SubscriptionForm(FlaskForm):

    email = StringField('Email', validators=[DataRequired(), Email()])
    reason = TextAreaField('Why did the chicken cross the road?')
    submit = SubmitField('Subscribe')