from flask import render_template
from flaskblog.config import ConfigSendgrid
from sendgrid.helpers.mail import *
from flask import url_for


def send_subscription_token(subscription):
    token = subscription.get_reset_token()
    sg = ConfigSendgrid.sg
    from_email = ConfigSendgrid.from_email
    to_email = Email(subscription.email)
    subject = "verify subscription"
    url = url_for('main.reset_token', token=token, _external=True)
    print(url)
    content = Content("text/html", render_template('subscribe_email.html', url=url))
    mail = Mail(from_email, subject, to_email, content)
    response = sg.client.mail.send.post(request_body=mail.get())

"""
    content = Content("text/html", f'''To subscribe please click on ze link below:
            {url_for('main.reset_token', token=token, _external=True)}
            If you did not make this request then do the impossible: ignore this email so no changes will be made. Best Regards, Sander
            ''')
"""



"""
    domain = url_for('main.reset_token', token=token)
    print("domain " + domain)
    url = url_for('main.reset_token', token=token, external=True)
    print("url " + url)
    content = Content("text/html", render_template('subscribe_email.html', url=url))
"""


